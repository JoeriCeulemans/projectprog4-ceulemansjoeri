/*
 * Project S
 * Author: Joeri Ceulemans
 */
package Entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

// <editor-fold defaultstate="collapsed" desc=" Annotations ">
@Entity
@Table(name = "company")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Company.findAll", query = "SELECT c FROM Company c")
    , @NamedQuery(name = "Company.findById", query = "SELECT c FROM Company c WHERE c.id = :id")
    , @NamedQuery(name = "Company.findByName", query = "SELECT c FROM Company c WHERE c.name = :name")
    , @NamedQuery(name = "Company.findByStreet", query = "SELECT c FROM Company c WHERE c.street = :street")
    , @NamedQuery(name = "Company.findByHousenumber", query = "SELECT c FROM Company c WHERE c.housenumber = :housenumber")
    , @NamedQuery(name = "Company.findByZipcode", query = "SELECT c FROM Company c WHERE c.zipcode = :zipcode")
    , @NamedQuery(name = "Company.findByCity", query = "SELECT c FROM Company c WHERE c.city = :city")})
// </editor-fold>

public class Company implements Serializable {
    // <editor-fold defaultstate="collapsed" desc=" Private Members ">

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "Name")
    private String name;
    @Basic(optional = false)
    @Column(name = "Street")
    private String street;
    @Basic(optional = false)
    @Column(name = "Housenumber")
    private String housenumber;
    @Basic(optional = false)
    @Column(name = "Zipcode")
    private int zipcode;
    @Basic(optional = false)
    @Column(name = "City")
    private String city;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "companyID", fetch = FetchType.LAZY)
    private Collection<Contact> contactCollection;
    @JoinColumn(name = "SectorID", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Sector sectorID;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "companyID", fetch = FetchType.LAZY)
    private Collection<Internship> internshipCollection;
// </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Constructors ">

    public Company() {
    }
    
    public Company(String name, String street, String housenumber, int zipcode, String city, Sector sector) {
        this.name = name;
        this.street = street;
        this.housenumber = housenumber;
        this.zipcode = zipcode;
        this.city = city;
        this.sectorID = sector;
    }
// </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Getters ">

    public Integer getId() {
        return id;
    }
    
    public String getName() {
        return name;
    }
    
    public String getStreet() {
        return street;
    }
    
    public String getHousenumber() {
        return housenumber;
    }
    
    public int getZipcode() {
        return zipcode;
    }
    
    public String getCity() {
        return city;
    }
    
    @XmlTransient
    public Collection<Contact> getContactCollection() {
        return contactCollection;
    }
    
    public Sector getSectorID() {
        return sectorID;
    }
    
    @XmlTransient
    public Collection<Internship> getInternshipCollection() {
        return internshipCollection;
    }

// </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Setters ">
    public void setId(Integer id) {
        this.id = id;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public void setStreet(String street) {
        this.street = street;
    }
    
    public void setHousenumber(String housenumber) {
        this.housenumber = housenumber;
    }
    
    public void setZipcode(int zipcode) {
        this.zipcode = zipcode;
    }
    
    public void setCity(String city) {
        this.city = city;
    }
    
    public void setContactCollection(Collection<Contact> contactCollection) {
        this.contactCollection = contactCollection;
    }
    
    public void setSectorID(Sector sectorID) {
        this.sectorID = sectorID;
    }
    
    public void setInternshipCollection(Collection<Internship> internshipCollection) {
        this.internshipCollection = internshipCollection;
    }

// </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Implementations ">
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }
    
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Company)) {
            return false;
        }
        Company other = (Company) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return this.name;
    }
// </editor-fold>
    
}
