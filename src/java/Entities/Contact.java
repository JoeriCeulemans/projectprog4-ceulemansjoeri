/*
 * Project S
 * Author: Joeri Ceulemans
 */
package Entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

// <editor-fold defaultstate="collapsed" desc=" Annotations ">
@Entity
@Table(name = "contact")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Contact.findAll", query = "SELECT c FROM Contact c")
    , @NamedQuery(name = "Contact.findById", query = "SELECT c FROM Contact c WHERE c.id = :id")
    , @NamedQuery(name = "Contact.findByName", query = "SELECT c FROM Contact c WHERE c.name = :name")
    , @NamedQuery(name = "Contact.findByFirstName", query = "SELECT c FROM Contact c WHERE c.firstName = :firstName")
    , @NamedQuery(name = "Contact.findByEmailaddress", query = "SELECT c FROM Contact c WHERE c.emailaddress = :emailaddress")
    , @NamedQuery(name = "Contact.findByPhone", query = "SELECT c FROM Contact c WHERE c.phone = :phone")
    , @NamedQuery(name = "Contact.findByCompany", query = "SELECT c FROM Contact c WHERE c.companyID = :companyID")})

// </editor-fold>
public class Contact implements Serializable {
    
    // <editor-fold defaultstate="collapsed" desc=" Private Members ">

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "Name")
    private String name;
    @Basic(optional = false)
    @Column(name = "FirstName")
    private String firstName;
    @Basic(optional = false)
    @Column(name = "Emailaddress")
    private String emailaddress;
    @Basic(optional = false)
    @Column(name = "Phone")
    private String phone;
    @JoinColumn(name = "CompanyID", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Company companyID;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "contactID", fetch = FetchType.LAZY)
    private Collection<Internship> internshipCollection;

// </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Constructors ">
    public Contact() {
    }
    
    public Contact(String name, String firstName, String emailaddress, String phone, Company company) {
        this.name = name;
        this.firstName = firstName;
        this.emailaddress = emailaddress;
        this.phone = phone;
        this.companyID = company;
    }

// </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Getters ">
    public Integer getId() {
        return id;
    }
    
    public String getName() {
        return name;
    }
    
    public String getFirstName() {
        return firstName;
    }
    
    public String getEmailaddress() {
        return emailaddress;
    }
    
    public String getPhone() {
        return phone;
    }
    
    public Company getCompany() {
        return companyID;
    }
    
    @XmlTransient
    public Collection<Internship> getInternshipCollection() {
        return internshipCollection;
    }
// </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Setters ">
    public void setId(Integer id) {
        this.id = id;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    
    public void setEmailaddress(String emailaddress) {
        this.emailaddress = emailaddress;
    }
    
    public void setPhone(String phone) {
        this.phone = phone;
    }
    
    public void setCompany(Company companyID) {
        this.companyID = companyID;
    }
    
    public void setInternshipCollection(Collection<Internship> internshipCollection) {
        this.internshipCollection = internshipCollection;
    }

// </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Implementations ">
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }
    
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Contact)) {
            return false;
        }
        Contact other = (Contact) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return this.name + ", " + this.firstName + " (" + this.companyID.getName() + ")";
    }

// </editor-fold>
        
}
