/*
 * Project S
 * Author: Joeri Ceulemans
 */
package Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

// <editor-fold defaultstate="collapsed" desc=" Annotations ">
@Entity
@Table(name = "internship")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Internship.findAll", query = "SELECT i FROM Internship i")
    , @NamedQuery(name = "Internship.findAllAvailable", query = "SELECT i FROM Internship i WHERE i.studentID is null")
    , @NamedQuery(name = "Internship.findById", query = "SELECT i FROM Internship i WHERE i.id = :id")
    , @NamedQuery(name = "Internship.findByDescription", query = "SELECT i FROM Internship i WHERE i.description = :description")
    , @NamedQuery(name = "Internship.findByCompany", query = "SELECT i FROM Internship i WHERE i.companyID = :company")
    , @NamedQuery(name = "Internship.findByMotivation", query = "SELECT i FROM Internship i WHERE i.motivation = :motivation")})

// </editor-fold>
public class Internship implements Serializable {
    
    // <editor-fold defaultstate="collapsed" desc=" Private Members ">

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    
    @Basic(optional = false)
    @Column(name = "Description")
    private String description;
    
    @Basic(optional = true)
    @Column(name = "Motivation")
    private String motivation;
    
    @JoinColumn(name = "StudentID", referencedColumnName = "ID")
    @ManyToOne(optional = true, fetch = FetchType.LAZY)
    private Student studentID;
    
    @JoinColumn(name = "CategoryID", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Category categoryID;
    
    @JoinColumn(name = "CompanyID", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Company companyID;
    
    @JoinColumn(name = "ContactID", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Contact contactID;

// </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Constructors ">
    public Internship() {
        
    }
    
    public Internship(Company company, Student student, Category category, Contact contact, String description, String motivation) {
        this.companyID = company;
        this.studentID = student;
        this.categoryID = category;
        this.contactID = contact;
        this.description = description;
        this.motivation = motivation;
    }
    
    public Internship(Company company, Category category, Contact contact, String description) {
        this.companyID = company;
        this.categoryID = category;
        this.contactID = contact;
        this.description = description;
    }

// </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Getters ">
    public Integer getId() {
        return id;
    }
    
    public String getDescription() {
        return description;
    }
    
    public String getMotivation() {
        return motivation;
    }
    
    public Student getStudent() {
        return studentID;
    }
    
    public Category getCategory() {
        return categoryID;
    }
    
    public Company getCompany() {
        return companyID;
    }
    
    public Contact getContact() {
        return contactID;
    }

// </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Setters ">
    public void setId(Integer id) {
        this.id = id;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public void setMotivation(String motivation) {
        this.motivation = motivation;
    }
    
    public void setStudent(Student student) {
        this.studentID = student;
    }
    
    public void setCategory(Category category) {
        this.categoryID = category;
    }
    
    public void setCompany(Company company) {
        this.companyID = company;
    }
    
    public void setContact(Contact contact) {
        this.contactID = contact;
    }

// </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Implementations ">
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }
    
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Internship)) {
            return false;
        }
        Internship other = (Internship) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        String internship = this.companyID.toString() + " - " + this.categoryID.toString();
        if (this.studentID != null) {
            internship += " - " + this.studentID.toString();
        } else {
            internship += " ** Available **";
        }
        return internship;
    }

// </editor-fold>
    
}
