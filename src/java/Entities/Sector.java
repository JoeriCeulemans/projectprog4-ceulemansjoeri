/*
 * Project S
 * Author: Joeri Ceulemans
 */
package Entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

// <editor-fold defaultstate="collapsed" desc=" Annotations ">
@Entity
@Table(name = "sector")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Sector.findAll", query = "SELECT s FROM Sector s")
    , @NamedQuery(name = "Sector.findById", query = "SELECT s FROM Sector s WHERE s.id = :id")
    , @NamedQuery(name = "Sector.findByName", query = "SELECT s FROM Sector s WHERE s.name = :name")})

// </editor-fold>
public class Sector implements Serializable {
    
    // <editor-fold defaultstate="collapsed" desc=" Private Members ">

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "Name")
    private String name;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sectorID", fetch = FetchType.LAZY)
    private Collection<Company> companyCollection;

// </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Constructors ">
    public Sector() {
    }
    
    public Sector(String name) {
        this.name = name;
    }

// </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Getters ">
    public Integer getId() {
        return id;
    }
    
    public String getName() {
        return name;
    }
    
    @XmlTransient
    public Collection<Company> getCompanyCollection() {
        return companyCollection;
    }

// </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Setters ">
    public void setId(Integer id) {
        this.id = id;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public void setCompanyCollection(Collection<Company> companyCollection) {
        this.companyCollection = companyCollection;
    }

// </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Implementations ">
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }
    
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sector)) {
            return false;
        }
        Sector other = (Sector) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return this.name;
    }

// </editor-fold>
    
}
