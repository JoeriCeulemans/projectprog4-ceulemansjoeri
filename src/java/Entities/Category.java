/*
 * Project S
 * Author: Joeri Ceulemans
 */
package Entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

// <editor-fold defaultstate="collapsed" desc=" Annotations ">
@Entity
@Table(name = "category")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Category.findAll", query = "SELECT c FROM Category c")
    , @NamedQuery(name = "Category.findById", query = "SELECT c FROM Category c WHERE c.id = :id")
    , @NamedQuery(name = "Category.findByName", query = "SELECT c FROM Category c WHERE c.name = :name")})

// </editor-fold>
public class Category implements Serializable {
    
    // <editor-fold defaultstate="collapsed" desc=" Private Members ">

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "Naam")
    private String name;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "categoryID", fetch = FetchType.LAZY)
    private Collection<Internship> internshipCollection;

// </editor-fold>

    // <editor-fold defaultstate="collapsed" desc=" Constructors ">
    public Category() {
        
    }
    
    public Category(String name) {
        this.name = name;
    }

// </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Getters ">
    public Integer getId() {
        return id;
    }
    
    public String getName() {
        return name;
    }
    
    @XmlTransient
    public Collection<Internship> getInternshipCollection() {
        return internshipCollection;
    }

// </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Setters ">
    public void setId(Integer id) {
        this.id = id;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public void setInternshipCollection(Collection<Internship> internshipCollection) {
        this.internshipCollection = internshipCollection;
    }

// </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Implementations ">
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }
    
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Category)) {
            return false;
        }
        Category other = (Category) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return this.name;
    }

// </editor-fold>
    
}
