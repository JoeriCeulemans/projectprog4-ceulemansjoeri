/*
 * Project S
 * Author: Joeri Ceulemans
 */
package Entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

// <editor-fold defaultstate="collapsed" desc=" Annotations ">
@Entity
@Table(name = "student")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Student.findAll", query = "SELECT s FROM Student s")
    , @NamedQuery(name = "Student.findById", query = "SELECT s FROM Student s WHERE s.id = :id")
    , @NamedQuery(name = "Student.findByName", query = "SELECT s FROM Student s WHERE s.name = :name")
    , @NamedQuery(name = "Student.findByFirstName", query = "SELECT s FROM Student s WHERE s.firstName = :firstName")
    , @NamedQuery(name = "Student.findByEmailaddress", query = "SELECT s FROM Student s WHERE s.emailaddress = :emailaddress")})

// </editor-fold>
public class Student implements Serializable {
    
    // <editor-fold defaultstate="collapsed" desc=" Private Members ">

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "Name")
    private String name;
    @Basic(optional = false)
    @Column(name = "FirstName")
    private String firstName;
    @Basic(optional = false)
    @Column(name = "Emailaddress")
    private String emailaddress;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "studentID", fetch = FetchType.LAZY)
    private Collection<Internship> internshipCollection;

// </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Constructors ">
    public Student() {
    }
    
    public Student(String name, String firstName, String emailaddress) {
        this.name = name;
        this.firstName = firstName;
        this.emailaddress = emailaddress;
    }
// </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Getters ">

    public Integer getId() {
        return id;
    }
    
    public String getName() {
        return name;
    }
    
    public String getFirstName() {
        return firstName;
    }
    
    public String getEmailaddress() {
        return emailaddress;
    }
    
    @XmlTransient
    public Collection<Internship> getInternshipCollection() {
        return internshipCollection;
    }

// </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Setters ">
    public void setId(Integer id) {
        this.id = id;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    
    public void setEmailaddress(String emailaddress) {
        this.emailaddress = emailaddress;
    }
    
    public void setInternshipCollection(Collection<Internship> internshipCollection) {
        this.internshipCollection = internshipCollection;
    }

// </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Implementations ">
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }
    
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Student)) {
            return false;
        }
        Student other = (Student) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return this.name + ", " + this.firstName + " (" + this.getId() + ")";
    }
// </editor-fold>
    
}
