/*
 * Project S Web
 * Author: Joeri Ceulemans
 */
package ServletStudents;

import DAL.IProjectService;
import DAL.ProjectService;
import Entities.Internship;
import Entities.Student;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author jceulema
 */
public class Register_StudentServlet extends HttpServlet {

    private final IProjectService _service = new ProjectService();
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        // Sessie ophalen
        HttpSession session = request.getSession();
        
        // Internship en student ophalen uit sessie
        Internship selectedStudentInternship = (Internship)session.getAttribute("selectedStudentInternship");
        Student student = (Student)session.getAttribute("student");
        
        // Get request parameter motivation
        String motivation = request.getParameter("motivation");
        
        // Internship updaten met student en motivatie
        _service.updateInternshipWithStudent(selectedStudentInternship.getId(), motivation, student);
        Boolean isStudentInternshipUpdated = true;
        
        // Session variabele listOfStudentInternships (available) updaten
        List<Internship> listOfStudentInternships = _service.getAvailableInternships();
        session.setAttribute("listOfStudentInternships", listOfStudentInternships);
        session.setAttribute("isStudentInternshipUpdated", isStudentInternshipUpdated);
        
        // Forward to...
        RequestDispatcher rd = request.getRequestDispatcher("Students/ViewInternships.jsp");
        rd.forward(request, response);
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
