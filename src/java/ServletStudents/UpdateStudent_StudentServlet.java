/*
 * Project S Web
 * Author: Joeri Ceulemans
 */
package ServletStudents;

import DAL.IProjectService;
import DAL.ProjectService;
import Entities.Student;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author jceulema
 */
public class UpdateStudent_StudentServlet extends HttpServlet {

    private final IProjectService _service = new ProjectService();
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        // Get student from session
        HttpSession session = request.getSession();
        Student student = (Student)session.getAttribute("student");
        
        // Get request parameters
        String name = request.getParameter("student_name");
        String firstName = request.getParameter("student_firstName");
        String emailaddress = request.getParameter("student_emailaddress");
        
        // Update db-student
        _service.updateStudent(student.getId(), name, firstName, emailaddress);
        Boolean isStudentUpdated = true;
        
        // Update session-student
        student.setEmailaddress(emailaddress);
        student.setFirstName(firstName);
        student.setName(name);
        
        // Set session-student again
        session.setAttribute("student", student);
        session.setAttribute("isStudentUpdated", isStudentUpdated);
        
        RequestDispatcher rd = request.getRequestDispatcher("Students/ViewStudent.jsp");
        rd.forward(request, response);
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
