/*
 * Project S Web
 * Author: Joeri Ceulemans
 */
package ServletCompanies;

import DAL.IProjectService;
import DAL.ProjectService;
import Entities.Category;
import Entities.Company;
import Entities.Contact;
import Entities.Internship;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author jceulema
 */
public class AddInternship_CompanyServlet extends HttpServlet {

    private final IProjectService _service = new ProjectService();
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        // Get session
        HttpSession session = request.getSession();
        List<Category> listOfCategories = (List<Category>)session.getAttribute("listOfCategories");
        List<Contact> listOfContacts = (List<Contact>)session.getAttribute("listOfContacts");
        Company company = (Company)session.getAttribute("company");
        
        // Get request parameters
        
            // Description
            String description = request.getParameter("internship_description");
            
            // Contact
            String internship_contact = request.getParameter("internship_contact");
            Contact contact = listOfContacts.get(0);
            String contactName = "";

            for (Contact c : listOfContacts) {
                contactName = c.getFirstName() + " " + c.getName();
                if (contactName.equals(internship_contact)) {
                    contact = c;
                    break;
                }
            }

            // Category
            String internship_category = request.getParameter("internship_category");
            Category category = listOfCategories.get(0);
            String categoryName = "";

            for (Category c : listOfCategories) {
                categoryName = c.getName();
                if (categoryName.equals(internship_category)) {
                    category = c;
                    break;
                }
            }
        
        // Make internship
        Internship internship = new Internship(company, category, contact, description);
        
        // Add internship
        _service.saveInternship(internship);
        List<Internship> listOfCompanyInternships = _service.getCompanyInternships(company);
        
        // Set session attributes
        session.setAttribute("isInternshipAdded", true);
        session.setAttribute("listOfCompanyInternships", listOfCompanyInternships);
        
        RequestDispatcher rd = request.getRequestDispatcher("Companies/ViewInternships.jsp");
        rd.forward(request, response);
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}