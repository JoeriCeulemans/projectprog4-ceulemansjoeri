/*
 * Project S Web
 * Author: Joeri Ceulemans
 */
package ServletCompanies;

import DAL.IProjectService;
import DAL.ProjectService;
import Entities.Company;
import Entities.Contact;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author jceulema
 */
public class UpdateContact_CompanyServlet extends HttpServlet {

    private final IProjectService _service = new ProjectService();
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        // Get contact from session
        HttpSession session = request.getSession();
        Contact selectedContact = (Contact)session.getAttribute("selectedContact");
        Company company = (Company)session.getAttribute("company");
        
        // Get request parameters
        String name = request.getParameter("contact_name");
        String firstName = request.getParameter("contact_firstName");
        String emailaddress = request.getParameter("contact_emailaddress");
        String phone = request.getParameter("contact_phone");
        
        // Update db-contact
        _service.updateContact(selectedContact.getId(), name, firstName, emailaddress, phone, company);
        Boolean isContactUpdated = true;
        
        // Update session-contact
        selectedContact.setEmailaddress(emailaddress);
        selectedContact.setFirstName(firstName);
        selectedContact.setName(name);
        selectedContact.setPhone(phone);
        
        // Set session-contact again
        session.setAttribute("selectedContact", selectedContact);
        session.setAttribute("isContactUpdated", isContactUpdated);
        
        RequestDispatcher rd = request.getRequestDispatcher("Companies/ViewContacts.jsp");
        rd.forward(request, response);
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
