/*
 * Project S Web
 * Author: Joeri Ceulemans
 */
package ServletCompanies;

import DAL.IProjectService;
import DAL.ProjectService;
import Entities.Category;
import Entities.Company;
import Entities.Contact;
import Entities.Internship;
import Entities.Sector;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/*
 *
 * @author jceulema
 */
public class ViewInternshipList_CompanyServlet extends HttpServlet {

    private final IProjectService _service = new ProjectService();
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        // Session ophalen
        HttpSession session = request.getSession();
        
        // Alle bedrijven ophalen
        List<Company> listOfAllCompanies = _service.getAllCompanies();
        // Het bedrijf dat is ingelogd is de eerste in de lijst
        Company company = listOfAllCompanies.get(0);
        // Alle stageplaatsen van het ingelogde bedrijf ophalen
        List<Internship> listOfCompanyInternships = _service.getCompanyInternships(company);
        // Alle contactpersonen van het ingelogde bedrijf ophalen
        List<Contact> listOfContacts = _service.getCompanyContacts(company);
        // Alle sectoren ophalen
        List<Sector> listOfSectors = _service.getAllSectors();
        // isCompanyUpdated set variable
        Boolean isCompanyUpdated = false;
        // Alle categorien ophalen
        List<Category> listOfCategories = _service.getAllCategories();
        
        Contact contact = new Contact();
        
        
        // Set attributes
        session.setAttribute("listOfCompanyInternships", listOfCompanyInternships);
        session.setAttribute("company", company);
        session.setAttribute("listOfContacts", listOfContacts);
        session.setAttribute("listOfSectors", listOfSectors);
        session.setAttribute("isCompanyUpdated", isCompanyUpdated);
        session.setAttribute("listOfCategories", listOfCategories);
        
        RequestDispatcher rd = request.getRequestDispatcher("Companies/ViewInternships.jsp");
        rd.forward(request, response);
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
