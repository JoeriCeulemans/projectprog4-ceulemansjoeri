/*
 * Project S Web
 * Author: Joeri Ceulemans
 */
package ServletCompanies;

import DAL.IProjectService;
import DAL.ProjectService;
import Entities.Company;
import Entities.Contact;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author jceulema
 */
public class AddContact_CompanyServlet extends HttpServlet {

    private final IProjectService _service = new ProjectService();
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        // Get session
        HttpSession session = request.getSession();
        Company company = (Company)session.getAttribute("company");
        
        // Get request parameters
        String contactName = request.getParameter("contact_name");
        String contactFirstName = request.getParameter("contact_firstName");
        String contactEmailaddress = request.getParameter("contact_emailaddress");
        String contactPhone = request.getParameter("contact_phone");
        
        // Make new contact
        Contact contact = new Contact(contactName, contactFirstName, contactEmailaddress, contactPhone, company);
        
        // Save it in the database
        _service.saveContact(contact);
        List<Contact> listOfContacts = _service.getCompanyContacts(company);
        
        // Set session attributes
        session.setAttribute("isContactAdded", true);
        session.setAttribute("listOfContacts", listOfContacts);
        
        RequestDispatcher rd = request.getRequestDispatcher("Companies/ViewContacts.jsp");
        rd.forward(request, response);
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
