/*
 * Project S Web
 * Author: Joeri Ceulemans
 */
package ServletCompanies;

import DAL.IProjectService;
import DAL.ProjectService;
import Entities.Company;
import Entities.Sector;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author jceulema
 */
public class UpdateCompany_CompanyServlet extends HttpServlet {

    private final IProjectService _service = new ProjectService();
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        // Get student from session
        HttpSession session = request.getSession();
        Company company = (Company)session.getAttribute("company");
        List<Sector> listOfSectors = (List<Sector>)session.getAttribute("listOfSectors");
        
        // Get request parameters
        String name = request.getParameter("company_name");
        String street = request.getParameter("company_street");
        String housenumber = request.getParameter("company_housenumber");
        int zipcode = Integer.parseInt(request.getParameter("company_zipcode"));
        String city = request.getParameter("company_city");
        String selectedSectorString = request.getParameter("company_sector");
        Sector sector = listOfSectors.get(1);
        String sectorName = "";
        
        for (Sector s : listOfSectors) {
            sectorName = s.getName();
            if (sectorName.equals(selectedSectorString)) {
                sector = s;
                break;
            }
        }
        
        // Update db-company
        _service.updateCompany(company.getId(), name, street, housenumber, zipcode, city, sector);
        Boolean isCompanyUpdated = true;
        
        // Update session-company
        company.setName(name);
        company.setStreet(street);
        company.setHousenumber(housenumber);
        company.setZipcode(zipcode);
        company.setCity(city);
        company.setSectorID(sector);
        
        // Set session-company again
        session.setAttribute("company", company);
        session.setAttribute("isCompanyUpdated", isCompanyUpdated);
        
        RequestDispatcher rd = request.getRequestDispatcher("Companies/ViewCompany.jsp");
        rd.forward(request, response);
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
}
