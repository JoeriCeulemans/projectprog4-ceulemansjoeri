/*
 * Project S Web
 * Author: Joeri Ceulemans
 */
package ServletCompanies;

import Entities.Internship;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author jceulema
 */
public class ViewInternship_CompanyServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        // Get internshipId from request
        Integer internshipId = Integer.parseInt(request.getParameter("companyInternshipId"));
        
        // Get internship from list
        List<Internship> listOfCompanyInternships =(List<Internship>)request.getSession().getAttribute("listOfCompanyInternships");
        
            // Option A --> Lambda
            // --> Optional<Internship> --> "Can not be cast to Internship"
            //Optional<Internship> selectedCompanyInternshipA;
            //selectedCompanyInternshipA = listOfCompanyInternships.stream().filter(o -> o.getId().equals(internshipId)).findFirst();

            // Option B --> Foreach
            Internship selectedCompanyInternshipB = new Internship();
            for (Internship internship : listOfCompanyInternships) {
                if (internshipId == internship.getId()) {
                    selectedCompanyInternshipB = internship;
                    break;
                }
            }
        
        HttpSession session = request.getSession();
        session.setAttribute("selectedCompanyInternship", selectedCompanyInternshipB);
        
        RequestDispatcher rd = request.getRequestDispatcher("Companies/ViewInternship.jsp");
        rd.forward(request, response);
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
