/*
 * Project S
 * Author: Joeri Ceulemans
 */
package DAL;

import Entities.Category;
import Entities.Company;
import Entities.Contact;
import Entities.Internship;
import Entities.Sector;
import Entities.Student;
import java.util.List;

public interface IProjectService {
    
    // <editor-fold defaultstate="collapsed" desc=" Get Objects ">

    List<Internship> getAllInternships();
    
    List<Internship> getAvailableInternships();
    
    List<Internship> getCompanyInternships(Company company);

    List<Student> getAllStudents();
    
    List<Student> getStudentByEmailaddress(String emailaddress);

    List<Company> getAllCompanies();
    
    List<Company> getCompanyByName(String name);

    List<Contact> getAllContacts();
    
    List<Contact> getCompanyContacts(Company company);
    
    List<Contact> getContactByEmail(String email);

    List<Category> getAllCategories();
    
    List<Category> getCategoryByName(String naam);

    List<Sector> getAllSectors();
    
    List<Sector> getSectorByName(String naam);

// </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Save Objects ">
    void saveInternship(Internship internship);

    void saveStudent(Student student);
    
    void saveCompany(Company company);

    void saveContact(Contact contact);
    
    void saveCategory(Category category);

    void saveSector(Sector sector);

// </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Update Objects ">
    void updateInternship(int id, Category category, Company company, Contact contact, String description, String motivation, Student student);
    
    void updateInternshipWithStudent(int id, String motivation, Student student);
    
    void updateInternshipForCompany(int id, String description, Contact contact, Category category);
    
    void updateStudent(int id, String name, String firstName, String emailAddress);
    
    void updateCompany(int id, String name, String street, String houseNumber, int Zipcode, String city, Sector sector);

    void updateContact(int id, String name, String firstName, String emailAddress, String phoneNumber, Company company);
    
    void updateCategory(int id, String name);

    void updateSector(int id, String name);

// </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Delete Objects ">
    void deleteInternship(int id);

    void deleteStudent(int id);
    
    void deleteCompany(int id);

    void deleteContact(int id);
    
    void deleteCategory(int id);

    void deleteSector(int id);

// </editor-fold>
    
}
