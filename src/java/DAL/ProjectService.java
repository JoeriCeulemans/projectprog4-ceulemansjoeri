/*
 * Project S
 * Author: Joeri Ceulemans
 */
package DAL;

import Entities.Category;
import Entities.Company;
import Entities.Contact;
import Entities.Internship;
import Entities.Sector;
import Entities.Student;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class ProjectService implements IProjectService {

    // <editor-fold defaultstate="collapsed" desc=" Private Members ">
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("Project_S_WebPU");
    private EntityManager em = emf.createEntityManager();
    

// </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Get Objects ">

    @Override
    public List<Internship> getAllInternships() {
        return em.createNamedQuery("Internship.findAll", Internship.class).getResultList();
    }
    
    @Override
    public List<Internship> getAvailableInternships() {
        return em.createNamedQuery("Internship.findAllAvailable", Internship.class).getResultList();
    }
    
    @Override
    public List<Internship> getCompanyInternships(Company company) {
        return em.createNamedQuery("Internship.findByCompany", Internship.class).setParameter("company", company).getResultList();
    }
    
    @Override
    public List<Student> getAllStudents() {
        return em.createNamedQuery("Student.findAll", Student.class).getResultList();
    }
    
    @Override
    public List<Student> getStudentByEmailaddress(String emailaddress) {
        return em.createNamedQuery("Student.findByEmailaddress", Student.class).setParameter("emailaddress", emailaddress).getResultList();
    }
    
    @Override
    public List<Company> getAllCompanies() {
        return em.createNamedQuery("Company.findAll", Company.class).getResultList();
    }
    
    @Override
    public List<Company> getCompanyByName(String name) {
        return em.createNamedQuery("Company.findByName", Company.class).setParameter("name", name).getResultList();
    }
    
    @Override
    public List<Contact> getAllContacts() {
        return em.createNamedQuery("Contact.findAll", Contact.class).getResultList();
    }
    
    @Override
    public List<Contact> getCompanyContacts(Company company) {
        return em.createNamedQuery("Contact.findByCompany", Contact.class).setParameter("companyID", company).getResultList();
    }
    
    @Override
    public List<Contact> getContactByEmail(String email) {
        return em.createNamedQuery("Contact.findByEmailaddress", Contact.class).setParameter("emailaddress", email).getResultList();
    }
    
    @Override
    public List<Category> getAllCategories() {
        return em.createNamedQuery("Category.findAll", Category.class).getResultList();
    }
    
    @Override
    public List<Category> getCategoryByName(String name) {
        return em.createNamedQuery("Category.findByName", Category.class).setParameter("name", name).getResultList();
    }
    
    @Override
    public List<Sector> getAllSectors() {
        return em.createNamedQuery("Sector.findAll", Sector.class).getResultList();
    }
    
    @Override
    public List<Sector> getSectorByName(String naam) {
        return em.createNamedQuery("Sector.findByName", Sector.class).setParameter("name", naam).getResultList();
    }

// </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Save Objects ">
    @Override
    public void saveInternship(Internship internship) {
        em.getTransaction().begin();
        em.persist(internship);
        em.getTransaction().commit();
    }
    
    @Override
    public void saveStudent(Student student) {
        em.getTransaction().begin();
        em.persist(student);
        em.getTransaction().commit();
    }
    
    @Override
    public void saveCompany(Company company) {
        em.getTransaction().begin();
        em.persist(company);
        em.getTransaction().commit();
    }
    
    @Override
    public void saveContact(Contact contact) {
        em.getTransaction().begin();
        em.persist(contact);
        em.getTransaction().commit();
    }
    
    @Override
    public void saveCategory(Category category) {
        em.getTransaction().begin();
        em.persist(category);
        em.getTransaction().commit();
    }
    
    @Override
    public void saveSector(Sector sector) {
        em.getTransaction().begin();
        em.persist(sector);
        em.getTransaction().commit();
    }

// </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Update Objects ">
    @Override
    public void updateCategory(int id, String name) {
        Category category = em.find(Category.class, id);
        em.getTransaction().begin();
        category.setName(name);
        em.getTransaction().commit();
    }
    
    @Override
    public void updateSector(int id, String name) {
        Sector sector = em.find(Sector.class, id);
        em.getTransaction().begin();
        sector.setName(name);
        em.getTransaction().commit();
    }
    
    @Override
    public void updateInternship(int id, Category category, Company company, Contact contact, String description, String motivation, Student student) {
        Internship internship = em.find(Internship.class, id);
        em.getTransaction().begin();
        internship.setCategory(category);
        internship.setCompany(company);
        internship.setContact(contact);
        internship.setDescription(description);
        internship.setMotivation(motivation);
        internship.setStudent(student);
        em.getTransaction().commit();
    }
    
    @Override
    public void updateInternshipWithStudent(int id, String motivation, Student student) {
        Internship internship = em.find(Internship.class, id);
        em.getTransaction().begin();
        internship.setMotivation(motivation);
        internship.setStudent(student);
        em.getTransaction().commit();
    }
    
    @Override
    public void updateInternshipForCompany(int id, String description, Contact contact, Category category) {
        Internship internship = em.find(Internship.class, id);
        em.getTransaction().begin();
        internship.setCategory(category);
        internship.setContact(contact);
        internship.setDescription(description);
        em.getTransaction().commit();
    }
    
    @Override
    public void updateStudent(int id, String name, String firstName, String emailAddress) {
        Student student = em.find(Student.class, id);
        em.getTransaction().begin();
        student.setName(name);
        student.setFirstName(firstName);
        student.setEmailaddress(emailAddress);
        em.getTransaction().commit();
    }
    
    @Override
    public void updateCompany(int id, String name, String street, String houseNumber, int Zipcode, String city, Sector sector) {
        Company company = em.find(Company.class, id);
        em.getTransaction().begin();
        company.setName(name);
        company.setStreet(street);
        company.setHousenumber(houseNumber);
        company.setZipcode(Zipcode);
        company.setCity(city);
        company.setSectorID(sector);
        em.getTransaction().commit();
    }
    
    @Override
    public void updateContact(int id, String name, String firstName, String emailAddress, String phoneNumber, Company company) {
        Contact contact = em.find(Contact.class, id);
        em.getTransaction().begin();
        contact.setName(name);
        contact.setFirstName(firstName);
        contact.setEmailaddress(emailAddress);
        contact.setPhone(phoneNumber);
        contact.setCompany(company);
        em.getTransaction().commit();
    }

// </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Delete Objects ">
    @Override
    public void deleteInternship(int id) {
        Internship internship = em.find(Internship.class, id);
        em.getTransaction().begin();
        em.remove(internship);
        em.getTransaction().commit();
    }
    
    @Override
    public void deleteStudent(int id) {
        Student student = em.find(Student.class, id);
        em.getTransaction().begin();
        em.remove(student);
        em.getTransaction().commit();
    }
    
    @Override
    public void deleteCompany(int id) {
        Company company = em.find(Company.class, id);
        em.getTransaction().begin();
        em.remove(company);
        em.getTransaction().commit();
    }
    
    @Override
    public void deleteContact(int id) {
        Contact contact = em.find(Contact.class, id);
        em.getTransaction().begin();
        em.remove(contact);
        em.getTransaction().commit();
    }
    
    @Override
    public void deleteCategory(int id) {
        Category category = em.find(Category.class, id);
        em.getTransaction().begin();
        em.remove(category);
        em.getTransaction().commit();
    }
    
    @Override
    public void deleteSector(int id) {
        Sector sector = em.find(Sector.class, id);
        em.getTransaction().begin();
        em.remove(sector);
        em.getTransaction().commit();
    }

// </editor-fold>

    

    

    

    

}
