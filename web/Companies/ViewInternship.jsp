<!--
 * Project S Web
 * Author: Joeri Ceulemans
-->

<%@page import="Entities.Category"%>
<%@page import="java.util.List"%>
<%@page import="Entities.Contact"%>
<%@page import="Entities.Internship"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    Internship internship = (Internship) session.getAttribute("selectedCompanyInternship");
    List<Category> listOfCategories = (List<Category>) session.getAttribute("listOfCategories");
    List<Contact> listOfContacts = (List<Contact>) session.getAttribute("listOfContacts");
    String internshipCategory = internship.getCategory().getName();
    String internshipDescription = internship.getDescription();
    Contact internshipContact = internship.getContact();
%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <script src="http://code.jquery.com/jquery.min.js"></script>
        <script src="http://getbootstrap.com/dist/js/bootstrap.js"></script>
        <script src="http://1000hz.github.io/bootstrap-validator/dist/validator.min.js"></script>
        <title>Project S - Internship details</title>
    </head>
    <body>
        <div style="margin:20px;">
            <ul class="nav nav-tabs">
                <li role="presentation"><a href="index.html">Home</a></li>
                <li role="presentation"><a href="Companies/ViewCompany.jsp">Company</a></li>
                <li role="presentation"><a href="ViewContacts">Contacts</a></li>
                <li role="presentation"><a href="ViewCompanyInternships">Internships</a></li>
                <li role="presentation" class="active"><a href="#">Internship details</a></li>
            </ul>
        </div>

        <div class="col-lg-6 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-4 col-xs-4 col-xs-offset-2">

            <form data-toggle="validator" role="form" METHOD="POST" action="UpdateInternship">

                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-info-sign"></i></span>
                            <%--<input id="description" type="text" class="form-control" name="description" value="<%=internshipDescription%>">--%>
                        <textarea class="form-control" rows="4" style="resize:none;" name="internship_description" required placeholder="A brief description..."><%=internshipDescription%></textarea>
                    </div>
                    <span class="help-block with-errors"></span>
                </div>

                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <select class="form-control" id="internship_contact" name="internship_contact">
                            <%
                                for (Contact contact : listOfContacts) {
                                    if (contact.equals(internship.getContact())) {
                            %>
                            <option selected="selected">
                                <%
                                } else {
                                %>
                            <option>
                                <%
                                    }
                                %>
                                <%= contact.getFirstName() + " "%>
                                <%= contact.getName()%>
                            </option>
                            <%
                                }
                            %>
                        </select>
                    </div>
                    <span class="help-block with-errors"></span>
                </div>

                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-copyright-mark"></i></span>
                        <select class="form-control" id="internship_category" name="internship_category">
                            <%
                                for (Category category : listOfCategories) {
                                    if (category.equals(internship.getCategory())) {
                            %>
                            <option selected="selected">
                                <%
                                } else {
                                %>
                            <option>
                                <%
                                    }
                                %>
                                <%=category.getName()%>
                            </option>
                            <%
                                }
                            %>
                        </select>
                    </div>
                    <span class="help-block with-errors"></span>
                </div>


                <div class="form-group">        
                    <p class="text-center"><button type="submit" class="btn btn-info">Update</button></p>
                </div>
            </form>
            <br>
            <p class="text-center"><small>Joeri Ceulemans &copy; 2018</small></p>
        </div>
    </body>
</html>
