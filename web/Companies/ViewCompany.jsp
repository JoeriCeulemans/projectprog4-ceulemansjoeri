<!--
 * Project S Web
 * Author: Joeri Ceulemans
-->

<%@page import="java.util.List"%>
<%@page import="Entities.Sector"%>
<%@page import="Entities.Company"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    Company company = (Company) session.getAttribute("company");
    List<Sector> listOfSectors = (List<Sector>) session.getAttribute("listOfSectors");

    Boolean message = false;
    try {
        if ((Boolean) session.getAttribute("isCompanyUpdated")) {
            message = true;
            request.getSession().setAttribute("isCompanyUpdated", false);
        } else {
            message = false;
        }
    } catch (Exception e) {
        System.out.println(e.getMessage());
    }
%>
<!DOCTYPE html>
<html>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="http://code.jquery.com/jquery.min.js"></script>

    <script src="http://getbootstrap.com/dist/js/bootstrap.js"></script>
    <script src="http://1000hz.github.io/bootstrap-validator/dist/validator.min.js"></script>
    <title>Project S - Company details</title>

    <body>
        <div style="margin:20px;">
            <ul class="nav nav-tabs">
                    <% if (message) {
                    %>
                <li role="presentation"><a href="index.html">Home</a></li>
                <li role="presentation" class="active"><a href="Companies/ViewCompany.jsp">Company</a></li>
                <li role="presentation"><a href="ViewContacts">Contacts</a></li>
                <li role="presentation"><a href="ViewCompanyInternships">Internships</a></li>
            </ul>
        </div>

        <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-4 col-xs-4 col-xs-offset-2">
            <div class="alert alert-success fade in alert-dismissable" style="margin-top:18px;">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                <strong>Success!</strong> Company details were updated...
            </div>
            <form data-toggle="validator" role="form" METHOD="POST" action="UpdateCompany">
                <%} else {
                %>
                <li role="presentation"><a href="../index.html">Home</a></li>
                <li role="presentation" class="active"><a href="Companies/ViewCompany.jsp">Company</a></li>
                <li role="presentation"><a href="../ViewContacts">Contacts</a></li>
                <li role="presentation"><a href="../ViewCompanyInternships">Internships</a></li>
                </ul>
        </div>

        <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-4 col-xs-4 col-xs-offset-2">

            <form data-toggle="validator" role="form" METHOD="POST" action="../UpdateCompany">
                <%
                    }
                %>
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-briefcase"></i></span>
                        <input id="company_name" type="text" class="form-control" name="company_name" placeholder="Name" required value="<%= company.getName()%>">
                    </div>
                    <span class="help-block with-errors"></span>
                </div>

                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-map-marker"></i></span>
                        <input id="company_street" type="text" class="form-control" name="company_street" placeholder="Street" required value="<%= company.getStreet()%>">
                    </div>
                    <span class="help-block with-errors"></span>
                </div>

                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-map-marker"></i></span>
                        <input id="company_housenumber" type="number" class="form-control" min="1" max="99999" name="company_housenumber" placeholder="Housenumber" required value="<%= company.getHousenumber()%>">
                    </div>
                    <span class="help-block with-errors"></span>
                </div>

                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-map-marker"></i></span>
                        <input id="company_zipcode" type="number" min="1000" max="9999" class="form-control" name="company_zipcode" placeholder="Zipcode" required value="<%= company.getZipcode()%>">
                    </div>
                    <span class="help-block with-errors"></span>
                </div>

                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-map-marker"></i></span>
                        <input id="company_city" type="text" class="form-control" name="company_city" placeholder="City" required value="<%= company.getCity()%>">
                    </div>
                    <span class="help-block with-errors"></span>
                </div>

                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-info-sign"></i></span>
                        <select class="form-control" id="company_sector" name="company_sector">
                            <%
                                for (Sector sector : listOfSectors) {
                                    if (sector.equals(company.getSectorID())) {
                            %>
                            <option selected="selected">
                                <%
                                } else {
                                %>
                            <option>
                                <%
                                    }
                                %>
                                <%=sector.getName()%>
                            </option>
                            <%
                                }
                            %>
                        </select>
                    </div>
                    <span class="help-block with-errors"></span>
                </div>

                <div class="form-group">        
                    <p class="text-center"><button type=submit class="btn btn-info">Update</button></p>
                </div>
            </form>
            <br>
            <p class="text-center"><small>Joeri Ceulemans &copy; 2018</small></p>
        </div>
    </body>
</html>
