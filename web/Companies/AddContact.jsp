<!--
 * Project S Web
 * Author: Joeri Ceulemans
-->

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <script src="http://code.jquery.com/jquery.min.js"></script>
        <script src="http://getbootstrap.com/dist/js/bootstrap.js"></script>
        <script src="http://1000hz.github.io/bootstrap-validator/dist/validator.min.js"></script>
        <title>Project S - Add a contact</title>
    </head>
    <body>
        <div style="margin:20px;">
            <ul class="nav nav-tabs">
                <li role="presentation"><a href="../index.html">Home</a></li>
                <li role="presentation"><a href="../Companies/ViewCompany.jsp">Company</a></li>
                <li role="presentation"><a href="../ViewContacts">Contacts</a></li>
                <li role="presentation" class="active"><a href="#">Add a contact</a></li>
                <li role="presentation"><a href="../ViewCompanyInternships">Internships</a></li>
            </ul>
        </div>
        
        <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-4 col-xs-4 col-xs-offset-2">
            
            <form data-toggle="validator" role="form" METHOD="POST" action="../AddContact">
                
                <div class="form-group has-feedback">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input id="contact_name" type="text" class="form-control" name="contact_name" placeholder="Name" required>
                    </div>
                    <span class="help-block with-errors"></span>
                </div>
                
                <div class="form-group has-feedback">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input id="contact_firstName" type="text" class="form-control" name="contact_firstName" placeholder="First name" required>
                    </div>
                    <span class="help-block with-errors"></span>
                </div>
                
                <div class="form-group has-feedback">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                        <input id="contact_emailaddress" type="email" class="form-control" name="contact_emailaddress" placeholder="Emailaddress" required>
                    </div>
                    <span class="help-block with-errors"></span>
                </div>
                
                <div class="form-group has-feedback">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
                        <input id="contact_phone" type="text" class="form-control" name="contact_phone" placeholder="Phone number" required>
                    </div>
                    <span class="help-block with-errors"></span>
                </div>
                
                <div class="form-group">        
                    <p class="text-center"><button type=submit class="btn btn-info">Add contact</button></p>
                </div>
            </form>
            <br>
            <p class="text-center"><small>Joeri Ceulemans &copy; 2018</small></p>
        </div>
    </body>
</html>
