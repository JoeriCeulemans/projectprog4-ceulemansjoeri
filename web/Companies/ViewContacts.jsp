<!--
 * Project S Web
 * Author: Joeri Ceulemans
-->

<%@page import="Entities.Internship"%>
<%@page import="java.util.List"%>
<%@page import="Entities.Contact"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    List<Contact> listOfContacts = (List<Contact>) session.getAttribute("listOfContacts");
    List<Internship> companyInternshipList = (List<Internship>) session.getAttribute("listOfCompanyInternships");

    Boolean msgUpdated = false;
    Boolean msgAdded = false;
    Boolean msgDeleted = false;
    try {
        if ((Boolean) session.getAttribute("isContactUpdated")) {
            msgUpdated = true;
            request.getSession().setAttribute("isContactUpdated", false);
        } else {
            msgUpdated = false;
        }
    } catch (Exception e) {
        System.out.println(e.getMessage());
    }

    try {
        if ((Boolean) session.getAttribute("isContactAdded")) {
            msgAdded = true;
            request.getSession().setAttribute("isContactAdded", false);
        } else {
            msgAdded = false;
        }
    } catch (Exception e) {
        System.out.println(e.getMessage());
    }

    try {
        if ((Boolean) session.getAttribute("isContactDeleted")) {
            msgDeleted = true;
            request.getSession().setAttribute("isContactDeleted", false);
        } else {
            msgDeleted = false;
        }
    } catch (Exception e) {
        System.out.println(e.getMessage());
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <title>Project S - Contactlist</title>
    </head>
    <body>
        <div style="margin:20px;" class="navbar-collapse collapse">
            <ul class="nav navbar-nav nav-tabs">
                <li role="presentation"><a href="index.html">Home</a></li>
                <li role="presentation"><a href="Companies/ViewCompany.jsp">Company</a></li>
                <li role="presentation" class="active"><a href="../ViewContacts">Contacts</a></li>
                <li role="presentation"><a href="ViewCompanyInternships">Internships</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <div class="btn-group" style="margin-right:30px;">
                        <a class="btn btn-success navbar-btn" href="Companies/AddContact.jsp">Add a contact</a>
                    </div>
                </li>
            </ul>
        </div>

        <% if (msgUpdated) {
        %>
        <div style="margin:20px;" class="alert alert-success fade in alert-dismissable" style="margin-top:18px;">
            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
            <strong>Success!</strong> Contact details were updated...
        </div>
        <%}
        %>

        <% if (msgAdded) {
        %>
        <div style="margin:20px;" class="alert alert-success fade in alert-dismissable" style="margin-top:18px;">
            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
            <strong>Success!</strong> Contact is added to the list...
        </div>
        <%}
        %>

        <% if (msgDeleted) {
        %>
        <div style="margin:20px;" class="alert alert-warning fade in alert-dismissable" style="margin-top:18px;">
            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
            <strong>Success!</strong> Contact is deleted...
        </div>
        <%}
        %>

        <div class="table-responsive" style="margin: 20px;">
            <table class="table table-striped table-hover" >
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Emailaddress</th>
                        <th>Phone</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <%
                        for (Contact contact : listOfContacts) {
                            Integer contactId = contact.getId();
                            String contactName = contact.getName();
                            String contactFirstName = contact.getFirstName();
                            String contactFullName = contactName + " " + contactFirstName;
                            String contactEmailaddress = contact.getEmailaddress();
                            String contactPhone = contact.getPhone();
                            Boolean isBezet = false;

                            for (Internship internship : companyInternshipList) {
                                if (internship.getContact().getId() == contactId) {
                                    isBezet = true;
                                    break;
                                } else {
                                    isBezet = false;
                                }
                            }
                    %>
                    <tr>
                        <td><%= contactFullName%></td>
                        <td><%= contactEmailaddress%></td>
                        <td><%= contactPhone%></td>
                        <td>
                            <form METHOD=POST ACTION="ViewContact">
                                <input type = "hidden" name = "contactId" value="<%=contactId%>">
                                <button type="submit" class="btn btn-info">
                                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> More info
                                </button>
                            </form>
                        </td>
                        <td>
                            <%
                                if (isBezet == false) {
                            %>
                            <form METHOD=POST ACTION="DeleteContact">
                                <input type = "hidden" name = "contactId" value="<%=contactId%>">
                                <button type="submit" class="btn btn-danger">
                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                </button>
                            </form>

                            <%
                                }
                            %>
                        </td>
                    </tr>
                    <%                        }
                    %>
                </tbody>
            </table>
            <br>
            <p class="text-center"><small>Joeri Ceulemans &copy; 2018</small></p>
        </div>
    </body>
</html>
