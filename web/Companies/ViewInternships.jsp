<!--
 * Project S Web
 * Author: Joeri Ceulemans
-->

<%@page import="Entities.Student"%>
<%@page import="java.util.List"%>
<%@page import="Entities.Internship"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    List<Internship> companyInternshipList = (List<Internship>) session.getAttribute("listOfCompanyInternships");

    Boolean msgUpdated = false;
    Boolean msgAdded = false;
    Boolean msgDeleted = false;
    try {
        if ((Boolean) session.getAttribute("isCompanyInternshipUpdated")) {
            msgUpdated = true;
            request.getSession().setAttribute("isCompanyInternshipUpdated", false);
        } else {
            msgUpdated = false;
        }
    } catch (Exception e) {
        System.out.println(e.getMessage());
    }

    try {
        if ((Boolean) session.getAttribute("isInternshipAdded")) {
            msgAdded = true;
            request.getSession().setAttribute("isInternshipAdded", false);
        } else {
            msgAdded = false;
        }
    } catch (Exception e) {
        System.out.println(e.getMessage());
    }

    try {
        if ((Boolean) session.getAttribute("isInternshipDeleted")) {
            msgDeleted = true;
            request.getSession().setAttribute("isInternshipDeleted", false);
        } else {
            msgDeleted = false;
        }
    } catch (Exception e) {
        System.out.println(e.getMessage());
    }

%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <title>Project S</title>
    </head>
    <body>
        <div style="margin:20px;" class="navbar-collapse collapse">
            <ul class="nav navbar-nav nav-tabs">
                <li role="presentation"><a href="index.html">Home</a></li>
                <li role="presentation"><a href="Companies/ViewCompany.jsp">Company</a></li>
                <li role="presentation"><a href="ViewContacts">Contacts</a></li>
                <li role="presentation" class="active"><a href="#">Internships</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <div class="btn-group" style="margin-right:30px;">
                        <a class="btn btn-success navbar-btn" href="Companies/AddInternship.jsp">Add an internship</a>
                    </div>
                </li>
            </ul>
        </div>

        <% if (msgUpdated) {
        %>
        <div style="margin:20px;" class="alert alert-success fade in alert-dismissable" style="margin-top:18px;">
            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
            <strong>Success!</strong> Internship details were updated...
        </div>
        <%}
        %>

        <% if (msgAdded) {
        %>
        <div style="margin:20px;" class="alert alert-success fade in alert-dismissable" style="margin-top:18px;">
            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
            <strong>Success!</strong> Internship is added to the list...
        </div>
        <%}
        %>

        <% if (msgDeleted) {
        %>
        <div style="margin:20px;" class="alert alert-warning fade in alert-dismissable" style="margin-top:18px;">
            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
            <strong>Success!</strong> Internship is deleted...
        </div>
        <%}
        %>

        <div class="table-responsive" style="margin: 20px;">
            <table class="table table-striped table-hover" >
                <thead>
                    <tr>
                        <th>Category</th>
                        <th>Contact</th>
                        <th>Description</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <%
                        for (Internship intern : companyInternshipList) {
                            Integer internshipId = intern.getId();
                            String categoryName = intern.getCategory().toString();
                            String contactName = intern.getContact().getName();
                            String contactFirstName = intern.getContact().getFirstName();
                            String contactFullName = contactName + " " + contactFirstName;
                            String description = intern.getDescription();
                            Student student = intern.getStudent();
                            Boolean isBezet;
                            if (student == null) {
                                isBezet = false;
                            } else {
                                isBezet = true;
                            }
                    %>
                    <tr>
                        <td><%= categoryName%></td>
                        <td><%= contactFullName%></td>
                        <td><%= description%></td>
                        <td>
                            <form METHOD=POST ACTION="ViewInternshipDetailsCompany">
                                <input type = "hidden" name = "companyInternshipId" value="<%=internshipId%>">
                                <button type="submit" class="btn btn-info">
                                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> More info
                                </button>
                            </form>
                        </td>
                        <td>
                            <%
                                if (isBezet == false) {
                            %>
                            <form METHOD=POST ACTION="DeleteInternship">
                                <input type = "hidden" name = "companyInternshipId" value="<%=internshipId%>">
                                <button type="submit" class="btn btn-danger">
                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                </button>
                            </form>

                            <%
                                }
                            %>
                        </td>
                    </tr>
                    <%                        }
                    %>
                </tbody>
            </table>
            <br>
            <p class="text-center"><small>Joeri Ceulemans &copy; 2018</small></p>
        </div>
    </body>
</html>
