<!--
 * Project S Web
 * Author: Joeri Ceulemans
-->

<%@page import="java.util.List"%>
<%@page import="Entities.Internship"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    Internship selectedStudentInternship = (Internship) session.getAttribute("selectedStudentInternship");
    String internshipCategory = selectedStudentInternship.getCategory().getName();
    String internshipCompanyName = selectedStudentInternship.getCompany().getName();
    String internshipCompanyStreet = selectedStudentInternship.getCompany().getStreet();
    String internshipCompanyHousenumber = selectedStudentInternship.getCompany().getHousenumber();
    int internshipCompanyZipcode = selectedStudentInternship.getCompany().getZipcode();
    String internshipCompanyCity = selectedStudentInternship.getCompany().getCity();
    String internshipDescription = selectedStudentInternship.getDescription();
%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <script src="http://code.jquery.com/jquery.min.js"></script>
        <script src="http://getbootstrap.com/dist/js/bootstrap.js"></script>
        <script src="http://1000hz.github.io/bootstrap-validator/dist/validator.min.js"></script>
        <title>Project S - Internship details</title>
    </head>
    <body>
        <div style="margin:20px;">
            <ul class="nav nav-tabs">
                <li role="presentation"><a href="index.html">Home</a></li>
                <li role="presentation"><a href="Students/ViewStudent.jsp">Student</a></li>
                <li role="presentation"><a href="ViewAvailableInternships">Internships</a></li>
                <li role="presentation" class="active"><a href="#">Internship details</a></li>
            </ul>
        </div>

        <div style="margin:20px;">
            <p>If you would like to register, please fill out a brief motivation.<small> (Max 500 char)</small></p>
        </div>

        <div class="col-lg-6 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-4 col-xs-4 col-xs-offset-2">

            <form data-toggle="validator" role="form" METHOD="POST" action="RegisterInternship">

                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon" style="min-width:100px;"><small>Category</small></span>
                        <input id="category" type="text" class="form-control" name="category" value="<%=internshipCategory%>" readonly>
                    </div>
                    <span class="help-block with-errors"></span>
                </div>

                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon" style="min-width:100px;"><small>Company</small></span>
                        <input id="name" type="text" class="form-control" name="name" value="<%=internshipCompanyName%>" readonly>
                    </div>
                    <span class="help-block with-errors"></span>
                </div>

                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon" style="min-width:100px;"><small>Street</small></span>
                        <input id="street" type="text" class="form-control" name="street" value="<%=internshipCompanyStreet%>" readonly>
                    </div>
                    <span class="help-block with-errors"></span>
                </div>

                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon" style="min-width:100px;"><small>Housenumber</small></span>
                        <input id="housenumber" type="text" class="form-control" name="housenumber" value="<%=internshipCompanyHousenumber%>" readonly>
                    </div>
                    <span class="help-block with-errors"></span>
                </div>

                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon" style="min-width:100px;"><small>Zipcode</small></span>
                        <input id="zipcode" type="text" class="form-control" name="zipcode" value="<%= internshipCompanyZipcode%>" readonly>
                    </div>
                    <span class="help-block with-errors"></span>
                </div>

                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon" style="min-width:100px;"><small>City</small></span>
                        <input id="city" type="text" class="form-control" name="city" value="<%=internshipCompanyCity%>" readonly>
                    </div>
                    <span class="help-block with-errors"></span>
                </div>

                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon" style="min-width:100px;"><small>Description</small></span>
                        <%--<input id="category" type="text" class="form-control" name="category" value="<%=internshipDescription%>" readonly>--%>
                        <textarea class="form-control" rows="4" style="resize:none;" readonly><%=internshipDescription%></textarea>
                    </div>
                    <span class="help-block with-errors"></span>
                </div>

                <div class="form-group has-feedback">
                    <div class="input-group">
                        <span class="input-group-addon" style="min-width:100px;"><small>Motivation</small></span>
                        <%--<input id="motivation" type="text" style="height:200px" class="form-control" data-maxLength="500" name="motivation" placeholder="Type your motivation here..." required>--%>
                        <textarea class="form-control" maxlength="500" rows="4" placeholder="Type your motivation here..." name="motivation" style="resize:none;" required></textarea>
                    </div>
                    <span class="help-block with-errors"></span>
                </div>

                <div class="form-group">        
                    <p class="text-center"><button type="submit" class="btn btn-info">Register</button></p>
                </div>
            </form>
            <br>
            <p class="text-center"><small>Joeri Ceulemans &copy; 2018</small></p>
        </div>

    </body>
</html>
