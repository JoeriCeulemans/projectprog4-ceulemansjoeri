<!--
 * Project S Web
 * Author: Joeri Ceulemans
-->

<%@page import="Entities.Student"%>
<%@page import="java.util.List"%>
<%@page import="Entities.Internship"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    // HttpSession session = request.getSession();
    List<Internship> internshipList = (List<Internship>) session.getAttribute("listOfStudentInternships");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <title>Project S - Available internships</title>
    </head>
    <body>
        <div style="margin:20px;">
            <ul class="nav nav-tabs">
                <li role="presentation"><a href="index.html">Home</a></li>
                <li role="presentation"><a href="Students/ViewStudent.jsp">Student</a></li>
                <li role="presentation" class="active"><a href="ViewAvailableInternships">Internships</a></li>
            </ul>
        </div>

        <div style="margin:20px;">
            <p>Here you can find a list of available internships</p>
        </div>
        <div class="table-responsive" style="margin: 20px;">
            <table class="table table-striped table-hover" >
                <thead>
                    <!--
                    <tr>
                        <th><INPUT TYPE=TEXT NAME=category SIZE=15></th>
                        <th><INPUT TYPE=TEXT NAME=company SIZE=15></th>
                        <th><INPUT TYPE=TEXT NAME=description SIZE=15></th>
                        <th></th>
                    </tr>
                    -->
                    <tr>
                        <th>Category</th>
                        <th>Company</th>
                        <th>Description</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <%
                        for (Internship intern : internshipList) {
                            Integer internshipId = intern.getId();
                            String categoryName = intern.getCategory().toString();
                            String companyName = intern.getCompany().getName();
                            String description = intern.getDescription();
                            Student student = intern.getStudent();
                    %>
                    <tr>
                        <td><%= categoryName%></td>
                        <td><%= companyName%></td>
                        <td><%= description%></td>
                        <td>
                            <form METHOD=POST ACTION="ViewInternshipDetailsStudent">
                                <input type = "hidden" name = "internshipId" value="<%=internshipId%>">
                                <button type="submit" class="btn btn-info">
                                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> More info
                                </button>
                            </form>
                        </td>
                    </tr>
                    <%
                        }
                    %>
                </tbody>
            </table>
            <br>
            <p class="text-center"><small>Joeri Ceulemans &copy; 2018</small></p>
        </div>
    </body>
</html>
