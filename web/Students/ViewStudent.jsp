<!--
 * Project S Web
 * Author: Joeri Ceulemans
-->

<%@page import="Entities.Student"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    Student student = (Student) session.getAttribute("student");
    Boolean message = false;
    try {
        if ((Boolean) session.getAttribute("isStudentUpdated")) {
            message = true;
            request.getSession().setAttribute("isStudentUpdated", false);
        } else {
            message = false;
        }
    } catch (Exception e) {
        System.out.println(e.getMessage());
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <script src="http://code.jquery.com/jquery.min.js"></script>

        <script src="http://getbootstrap.com/dist/js/bootstrap.js"></script>
        <script src="http://1000hz.github.io/bootstrap-validator/dist/validator.min.js"></script>
        <title>Project S - Student details</title>
    </head>
    <body>

        <div style="margin:20px;">
            <ul class="nav nav-tabs">
                    <% if (message) {
                    %>
                    <li role="presentation"><a href="index.html">Home</a></li>
                    <li role="presentation" class="active"><a href="ViewStudent.jsp">Student</a></li>
                    <li role="presentation"><a href="ViewAvailableInternships">Internships</a></li>
            </ul>
        </div>

        <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-4 col-xs-4 col-xs-offset-2">
            <div class="alert alert-success fade in alert-dismissable" style="margin-top:18px;">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                <strong>Success!</strong> Student details were updated...
            </div>
            <form data-toggle="validator" role="form" METHOD="POST" action="UpdateStudentDetails">
                <%} else {
                %>
                <li role="presentation"><a href="../index.html">Home</a></li>
                <li role="presentation" class="active"><a href="ViewStudent.jsp">Student</a></li>
                <li role="presentation"><a href="../ViewAvailableInternships">Internships</a></li>
                </ul>
        </div>

        <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-4 col-xs-4 col-xs-offset-2">

            <form data-toggle="validator" role="form" METHOD="POST" action="../UpdateStudentDetails">
                <%
                    }
                %>

                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input id="student_name" type="text" class="form-control" name="student_name" placeholder="Name" required value="<%= student.getName()%>">
                    </div>
                    <span class="help-block with-errors"></span>
                </div>
                <br>

                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input id="student_firstName" type="text" class="form-control" name="student_firstName" placeholder="First name" required value="<%= student.getFirstName()%>">
                    </div>
                    <span class="help-block with-errors"></span>
                </div>
                <br>


                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                        <input id="student_emailaddress" type="email" class="form-control" name="student_emailaddress" placeholder="Emailaddress" required value="<%= student.getEmailaddress()%>">
                    </div>
                    <span class="help-block with-errors"></span>
                </div>
                <br>

                <div class="form-group">        
                    <p class="text-center"><button type=submit class="btn btn-info">Update</button></p>
                </div>
            </form>
            <br>
            <p class="text-center"><small>Joeri Ceulemans &copy; 2018</small></p>
        </div>
    </body>
</html>
